<?php

return [
    "Dashboard" => [
        "icon"  => "far fa-tachometer-alt-average",
        "links" => [["route" => "admin/dashboard", "label" => "dashboard"]],
    ],
];
