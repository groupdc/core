<?php

namespace Dcms\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;
use Input;
use Auth;

class BaseController extends Controller
{
    public function __construct(Request $request)
    {
        if (!$request->ajax() || ($request->ajax() && $request->has('_token'))) {
            //  	$this->beforeFilter('csrf', array('on'=>'post'));
        }

        if (isset(Auth::guard('dcms')->user()->id) &&  isset(Auth::guard('dcms')->user()->id) > 0) {
            if (session_id() == '') {
                session_start();
            }
            $_SESSION["admin"]["allow_ckfinder"] = true;
        }
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (! is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }
}
