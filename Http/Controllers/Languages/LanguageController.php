<?php

namespace Dcms\Core\Http\Controllers\Languages;

use Dcms\Core\Models\Countries\Country;
use Dcms\Core\Models\Languages\Language;

use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;
use DateTime;
use Route;

use Dcms\Core\Http\Controllers\BaseController;

class LanguageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view
        return View::make('dcms::settings/languages/index');
    }

    public function getDatatable()
    {
        return DataTables::queryBuilder(DB::connection("project")
                                                ->table("languages")
                                                ->select(
                                                    'languages.id',
                                                    'languages.language',
                                                    'languages.language_name',
                                                    (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/assets/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as country'))
                                                ))
                        ->addColumn('edit', '<form class="pull-right"> <a class="btn btn-xs btn-default" href="/admin/settings/languages/{{$id}}/edit"><i class="far fa-pencil"></i></a></form>')
                        ->rawColumns(['country','edit'])
                        ->make(true) ;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('dcms::settings/languages/form')
                    ->with('countryOptionValues', Country::OptionValueArray(false));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'language'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the validator
        if ($validator->fails()) {
            return Redirect::to('admin/settings/languages/create')
                ->withErrors($validator)
                ->withInput();
        //->withInput($request->except());
        } else {
            // store

            $Language = new Language;
            $Language->country_id = $request->get('country_id');
            $Language->language = $request->get('language');
            $Language->language_name = $request->get('language_name');
            $Language->country = strtoupper($this->getCountry($request->get('country_id')));
            $Language->save();

            // redirect
            Session::flash('message', 'Successfully created language!');
            return Redirect::to('admin/settings/languages');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the country
        $Language = Language::find($id);

        // show the edit form and pass the nerd
        return View::make('dcms::settings/languages/form')
                    ->with('Language', $Language)
                    ->with('countryOptionValues', Country::OptionValueArray(false));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'language'       => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/settings/languages/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store

            $Language = Language::find($id);
            $Language->country_id = $request->get('country_id');
            $Language->language = $request->get('language');
            $Language->language_name = $request->get('language_name');
            $Language->country = strtoupper($this->getCountry($request->get('country_id')));

            $Language->save();

            // redirect
            Session::flash('message', 'Successfully updated country!');
            return Redirect::to('admin/settings/languages');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        //$Country = Country::find($id);
        //$Country->delete();

        // redirect
        Session::flash('message', 'Sorry nothing has been deleted, ask your DBA for thorough delete!');
        return Redirect::to('admin/settings/countries');
    }


    private function getCountry($id)
    {
        $Country = Country::find($id);
        return $Country->country;
    }
}
