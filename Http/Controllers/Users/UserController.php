<?php

namespace Dcms\Core\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;
use Spatie\Permission\Models\Role;
use View;
use Validator;
use Auth;
use Redirect;
use Dcms\Core\Models\Users\User;
use Dcms\Core\Models\Languages\Language;
use DataTables;
use DB;
use Hash;
use Session;
use Mail;

class UserController extends Controller
{
    /**
     * Set permissions.
     *
     */
    public function __construct()
    {
        $this->middleware('permission:users-browse')->only('index');
        $this->middleware('permission:users-add')->only(['create', 'store']);
        $this->middleware('permission:users-edit')->only(['edit', 'update']);
        $this->middleware('permission:users-delete')->only('destroy');
    }

    protected function isPostRequest()
    {
        return request()->isMethod('post');
    }

    protected function getLoginValidator()
    {
        return Validator::make(request()->all(), [
            "username" => "required",
            "password" => "required",
        ]);
    }

    protected function getLoginCredentials()
    {
        return [
            "username" => request()->get("username"),
            "password" => request()->get("password"),
        ];
    }

    public function login()
    {
        if ($this->isPostRequest()) {
            $validator = $this->getLoginValidator();

            if ($validator->passes()) {
                $credentials = $this->getLoginCredentials();

                if (Auth::guard('dcms')->attempt($credentials)) {
                    if (session_id() == '') {
                        session_start();
                    }
                    $_SESSION["admin"]["allow_ckfinder"] = true;

                    $User = User::find(Auth::guard('dcms')->user()->id);
                    $User->last_login = date("Y-m-d H:i:s");
                    $User->save();
                    session(['overrule_default_by_country_id'=> 0]);
                    session(['overrule_default_by_language_id'=> intval($User->language_id)]);
                    if (session('overrule_default_by_language_id')!=0) {
                        session(['overrule_default_by_country_id'=> \Dcms\Core\Models\Languages\Language::where('id', intval(session('overrule_default_by_language_id')))->first()->country_id]);
                    }
                    return Redirect::intended("admin/dashboard");
                }

                return Redirect::back()->withErrors(["password" => ["Username and/or password invalid."]]);
            } else {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validator);
            }
        }

        return View::make("dcms::users/login");
    }

    public function profile()
    {
        $user = User::find(Auth::guard('dcms')->user()->id);
        $user->password = "";

        return View::make("dcms::users/profile")->with('user', $user);
    }

    public function updateProfile(Request $request)
    {
        $passwordError = false;
        if ($this->isPostRequest()) {
            $validatorrules = ["email" => "required|email"];

            if (trim($request->get("password")) <> "" || trim($request->get("newpassword")) <> "" || trim($request->get("newpasswordrepeat")) <> "") {
                $validateCurrentpassword = true;
                $validatorrules = array_merge($validatorrules, ["password" => "required", "newpassword" => "required|min:6", "newpasswordrepeat" => "same:newpassword"]);
                if (Hash::check($request->get('password'), Auth::guard('dcms')->user()->getAuthPassword()) !== true) {
                    $passwordError = true;
                }
            }

            $validator = Validator::make($request->all(), $validatorrules, [
                    "email"                    => "Email should be a valid email"
                    , "required"               => "The :attribute field is required"
                    , "password.required"      => "The current password is required"
                    , "newpassword.required"   => "A new password is required"
                    , "newpassword.min"        => "A new password needs minimum 6 characters"
                    , "newpasswordrepeat.same" => "the new password - repeat field needs to be the same as the new password",
                ]);

            if ($validator->passes() && $passwordError == false) {
                $user = User::find(Auth::guard('dcms')->user()->id);

                $user->name = $request->get('name');
                $user->email = $request->get('email');
                $user->language_id = ($request->has('language_id') ? intval($request->get('language_id')) : null);

                if (strlen(trim($request->get('newpassword'))) > 0) {
                    $user->password = Hash::make($request->get('newpassword'));
                }
                $user->save();


                Session::flash('message', 'Successfully updated profile!');

                return Redirect::to('admin/profile');
            } elseif ($passwordError == true) {
                return Redirect::back()
                    ->withInput()
                    ->withErrors(["error" => "Current password does not match"]);
            } else {
                return Redirect::back()
                    ->withInput()
                    ->withErrors($validator);
            }
        } else {
            return Redirect::to('admin/profile');
        }
    }

    public function logout()
    {
        Auth::guard('dcms')->logout();

        return Redirect::route("admin.login");
    }

    /*****************************************************
     * CRUD METHODS
     *******************************************************/
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        // get all the users
        //		$users = User::all();
        $users = User::paginate(5);

        // load the view and pass the users
        return View::make('dcms::users/index')
            ->with('users', $users);
    }

    public function getDatatable()
    {
        $datatable = DataTables::of(User::query());

        $datatable = $datatable->addColumn('edit', function ($user) {
            $edit = '<form method="POST" action="/admin/users/'.$user->id.'" accept-charset="UTF-8" class="pull-right"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">';
            if (Auth::user()->can('users-edit')) {
                $edit .= '<a class="btn btn-xs btn-default" href="/admin/users/'.$user->id.'/edit"><i class="far fa-pencil"></i></a>';
            }
            if (Auth::user()->can('users-delete')) {
                $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this article" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>';
            }
            $edit .= '</form>';
            return $edit;
        })->addColumn('roles', function ($user) {
            return $user->roles->pluck('name')->implode(', ');
        })->rawColumns(['edit']);

        return $datatable->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::all();
        $languages = Language::all();

        // load the create form (app/views/users/create.blade.php)
        return View::make('dcms::users/form')->with('roles', $roles)->with('languages', $languages);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name'     => 'required',
            'username' => 'required',
            'email'    => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the validator
        if ($validator->fails()) {
            return Redirect::to('admin/users/create')
                ->withErrors($validator)
                ->withInput();
        //->withInput($request->except());
        } else {
            // store
            $user = new User;
            $user->name = $request->get('name');
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->language_id = ($request->has('language_id') ? intval($request->get('language_id')) : null);
            $user->password = Hash::make($request->get('password'));
            $this->sendemail($request);
            $user->save();

            $user->syncRoles($request->get('roles'));

            // redirect
            Session::flash('message', 'Successfully created user!');

            return Redirect::to('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //
        // get the user
        $user = User::find($id);
        $user->password = "";

        $roles = Role::all();
        $languages = Language::all();

        // show the edit form and pass the nerd
        return View::make('dcms::users/form')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('languages', $languages);
    }

    /**
     * Sendemail will send the user his username and password
     *
     * @param obj $model
     */
    protected function sendemail(Request $request)
    {
        if ($request->get("sendemail") == "1") {
            /*****************************************
             * - MAIL SETTINGS -
             *****************************************/
            // I'm creating an array with user's info but most likely you can use $user->email or pass $user object to closure later
            $emailuser = [
                'email'    => $request->get('email'),
                'name'     => $request->get('name'),
                'username' => $request->get('username'),
                'password' => $request->get('password'),
            ];

            // the data that will be passed into the mail view blade template
            $data = [
                'name'     => 'name: <b>' . $emailuser['name'] . '</b>',
                'username' => 'username: <b>' . $emailuser['username'] . '</b>',
                'email'    => 'email: <b>' . $emailuser['email'] . '</b>',
                'password' => 'password: <b>' . $emailuser['password'] . '</b>',
            ];

            // use Mail::send function to send email passing the data and using the $user variable in the closure
            Mail::send('dcms::users/email', $data, function ($message) use ($emailuser) {
                $message->from('web@groupdc.be', 'New DCMS User');
                $message->to($emailuser['email'], $emailuser['name'])->subject('Welcome to My Laravel app!');
            });
            /*****************************************
             * - END MAIL  -
             *****************************************/
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            // store
            $user = User::find($id);
            $user->name = $request->get('name');
            $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->language_id = ($request->has('language_id') ? intval($request->get('language_id')) : null);
            if (strlen(trim($request->get('password'))) > 0) {
                $user->password = Hash::make($request->get('password'));
            }
            $this->sendemail($request);
            $user->save();

            $user->syncRoles($request->get('roles'));

            // redirect
            Session::flash('message', 'Successfully updated user!');

            return Redirect::to('admin/users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $user = User::find($id);
        $user->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the user!');

        return Redirect::to('admin/users');
    }

    /*****************************************************
     * END CRUD METHODS
     *******************************************************/
}
