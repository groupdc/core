<?php

namespace Dcms\Core\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use View;

class RoleController extends Controller
{
    /**
     * Set permissions.
     *
     */
    public function __construct()
    {
        $this->middleware('permission:users-browse')->only('index');
        $this->middleware('permission:users-edit')->only(['edit', 'update']);
        $this->middleware('permission:users-add')->only(['create', 'store']);
        $this->middleware('permission:users-delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('dcms::roles/index');
    }

    public function getDatatable()
    {
        $datatable = DataTables::of(Role::query());

        $datatable = $datatable->addColumn('edit', function ($role) {
            $edit = '<form method="POST" action="/admin/roles/'.$role->id.'" accept-charset="UTF-8" class="pull-right"><input name="_token" type="hidden" value="' . csrf_token() . '"><input name="_method" type="hidden" value="DELETE">';
            if (Auth::user()->can('users-edit')) {
                $edit .= '<a class="btn btn-xs btn-default" href="/admin/roles/'.$role->id.'/edit"><i class="far fa-pencil"></i></a>';
            }
            if (Auth::user()->can('users-delete')) {
                $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this role" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>';
            }
            $edit .= '</form>';
            return $edit;
        })->rawColumns(['edit']);

        return $datatable->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::where('level', 1)->get()->groupBy('module');

        return View::make('dcms::roles/form')
            ->with('permissions', $permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $role = Role::firstOrCreate(['name' => $request->get('name')]);

        $this->processRequest($request, $role);

        Session::flash('message', 'Successfully created role!');
        return redirect()->route('admin.roles.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::where('level', 1)->get()->groupBy('module');


        // show the edit form and pass the nerd
        return View::make('dcms::roles/form')
            ->with('role', $role)
            ->with('permissions', $permissions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->name = $request->get('name');
        $role->save();

        $this->processRequest($request, $role);

        Session::flash('message', 'Successfully updated role!');
        return redirect()->route('admin.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->syncPermissions([]);
        $role->users()->detach();

        $role->delete();

        Session::flash('message', 'Successfully deleted the role!');
        return redirect()->route('admin.roles.index');
    }

    private function processRequest($request, $role)
    {
        $permissions = Permission::find($request->get('permissions'));

        if ($permissions) {
            $module_permissions = Permission::whereIn('name', $permissions->pluck('module'))->where('level', 0)->get();
            $permissions = $permissions->merge($module_permissions);
        }

        $role->syncPermissions($permissions);
    }
}
