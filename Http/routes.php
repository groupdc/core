<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('timezones/{timezone}', 'CoreController@index');

    Route::group(array("prefix" => "admin"), function () {
        Route::get("/", function () {
            return Redirect::to("admin/login");
        });

        Route::any("/login", array( "as" => "admin.login", "uses" => "Users\UserController@login"));
        Route::any("/logout", array( "as" => "admin.logout", "uses" => "Users\UserController@logout"));

        //Route::group(array("before" => "auth.dcms"), function() {
        Route::group(['middleware' => 'auth:dcms'], function () {
            //DASHBOARD - CMS HOME
            Route::any("dashboard", array( "as" => "admin/dashboard", "uses" => "Dashboard\DashboardController@dashboard"));

            //USERS
            Route::group(array("before"=>"admin.dcms",'as'=>'admin.'), function () {
                Route::resource('users', 'Users\UserController');
                Route::any('users/api/table', array('as'=>'users.api.table', 'uses' => 'Users\UserController@getDatatable'));
            });

            //ROLES
            Route::group(array("before"=>"admin.dcms",'as'=>'admin.'), function () {
                Route::resource('roles', 'Roles\RoleController');
                Route::any('roles/api/table', array('as'=>'roles.api.table', 'uses' => 'Roles\RoleController@getDatatable'));
            });

            //PROFILE
            Route::any("profile", array( "as" => "admin/profile", "uses" => "Users\UserController@profile"));
            Route::any("profile/edit", array( "as" => "admin/profile/edit", "uses" => "Users\UserController@updateProfile"));

            //SETTINGS - SET UP EXTRA LANGUAGES
            Route::group(array("prefix" => "settings", "as"=>"admin.settings.", "before"=>"admin.dcms"), function () {
                Route::get('toggledefault', function () {
                    if (intval(Illuminate\Support\Facades\Request::instance()->get('language_id'))>0) {
                        session(['overrule_default_by_language_id'=> intval(Illuminate\Support\Facades\Request::instance()->get('language_id'))]);
                        session(['overrule_default_by_country_id'=> \Dcms\Core\Models\Languages\Language::where('id', intval(session('overrule_default_by_language_id')))->first()->country_id]);
                    } else {
                        session(['overrule_default_by_language_id'=> 0]);
                        session(['overrule_default_by_country_id'=> 0]);
			//x
                    }
                    return Redirect::back();
                });
               
                //COUNTRIES
                Route::group(array("prefix" => "countries"), function () {
                    Route::any('api/table', array('as'=>'countries.api.table', 'uses' => 'Settings\CountryController@getDatatable'));
                });

                //Route::get('countries/{country_id}/edit','Settings\CountryController@edit');
                //Route::get('countries/{country_id}/update',array('as'=>'admin.settings.countries.update', 'uses'=>'Settings\CountryController@update'));
                Route::resource('countries', 'Settings\CountryController');

                //LANGUAGES
                Route::group(array("prefix" => "languages","before"=>"admin.dcms"), function () {
                    Route::any('api/table', array('as'=>'languages.api.table', 'uses' => 'Settings\LanguageController@getDatatable'));
                });
                Route::resource('languages', 'Settings\LanguageController');
            });
            Route::any('settings', 'Settings\SettingController@index');
        });
    });
});
