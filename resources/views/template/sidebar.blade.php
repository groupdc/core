@section("sidebar")
    <div class="sidebar">
        <ul class="nav nav-sidebar">
            @foreach(Config::get('dcms_sidebar') as $group => $groupsettings )

                @php
                    // Filter links without permission
                    foreach ($groupsettings['links'] as $index => $link) {
                        if (array_key_exists('permission', $link) && !Auth::user()->can($link['permission'])) {
                            unset($groupsettings['links'][$index]);
                        }
                    }
                @endphp

                @if(count($groupsettings['links']) > 1)
                    <li class="dropdown">
                        <a href="{!! URL::to($groupsettings['links'][0]['route']) !!}"><i class="fa {{$groupsettings['icon']}}"></i><span style="margin-left:10px;">{{$group}}</span><b
                                    class="arrow far fa-angle-down"></b></a>
                        <ul class="dropdown-menu">
                            @foreach($groupsettings['links'] as $linkdetails)
                                <li><a href="{!! URL::to($linkdetails['route']) !!}">{{$linkdetails['label']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @elseif(count($groupsettings['links']) == 1)
                    <li>
                        <a href="{!! URL::to($groupsettings['links'][0]['route']) !!}">
                            <i class="fa {{$groupsettings['icon']}}"></i><span style="margin-left:10px;">{{$group}}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>

        <div class="text-right collapse-button">
            <button id="sidebar-collapse" class="btn btn-default" style="">
                <i class="far fa-angle-right"></i>
            </button>
        </div>
    </div>
@show
