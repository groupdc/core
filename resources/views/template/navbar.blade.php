@section("navbar")
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="{!! URL::to('admin/dashboard') !!}"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
            
            <li class="dropdown">
            
            @php 
            $languages = Dcms\Core\Models\Languages\Language::all()
            @endphp
            
                        <a href="">@if(intval(session('overrule_default_by_language_id')) <= 0)
                        <i class="far fa-globe-europe"></i> 
                            @else
                                <img src="/packages/Dcms/Core/images/flag-{{strtolower($languages->where('id',intval(session('overrule_default_by_language_id')))->first()->country)}}.svg" style="width:15px; height: auto;" >
                            @endif <span style="margin-left:5px;">Languages</span> 
                        <b class="arrow far fa-angle-down"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{!! URL::to('admin/settings/toggledefault?language_id=') !!}">
                                <i class="far fa-globe-europe"></i> All languages
                                </a>
                            </li>
                        @foreach($languages as $language) 
                            <li>
                                <a href="{!! URL::to('admin/settings/toggledefault?language_id='.$language->id) !!}">
                                    <img src='/packages/Dcms/Core/images/flag-{{strtolower($language->country)}}.svg' style="width:15px; height: auto; margin-right:5px;" > {{strtolower($language->language).'-'.strtoupper($language->country)}}
                                    </a>
                            </li>
                        @endforeach
                                                    </ul>
                    </li>



            <li>
                @can ('core')
                    <li class="dropdown"><a href="{!! URL::to('admin/settings') !!}"><i class="far fa-cog"></i> Settings</a>
                        <ul class="dropdown-menu">
                            <li><a href="{!! URL::to('admin/settings/countries') !!}"><i class="far fa-globe-europe"></i> Countries</a></li>
                            <li><a href="{!! URL::to('admin/settings/languages') !!}"><i class="far fa-flag"></i> Languages</a></li>
                        </ul>
                    </li>
                @endcan
                @can('users')
                    <li><a href="{!! URL::to('admin/users') !!}"><i class="far fa-user"></i> Users</a></li>
                    <li><a href="{!! URL::to('admin/roles') !!}"><i class="far fa-user-tag"></i> Roles</a></li>
                @endcan
            </ul>
            <ul class="nav navbar-nav">
                <li><a href="/"><b>Visit website</b></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="{!! URL::route('admin/profile') !!}">Logged in as <b>{{ Auth::guard('dcms')->user()->username }}</b></a></li>
                <li><a href="{!! URL::route('admin.logout') !!}"><i class="far fa-sign-out"></i> Logout</a></li>
            </ul>
        </div>
    </div>
@show
