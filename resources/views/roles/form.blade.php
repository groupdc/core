@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Roles</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/roles') !!}"><i class="far fa-user-tag"></i> Roles</a></li>
            @if(isset($role))
                <li class="active">Edit</li>
            @else
                <li class="active">Create</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    <h2>{{isset($role) ? 'Edit' : 'Create'}} Role</h2>

                    <form action="{{ isset($role) ? route('admin.roles.update', $role->id) : route('admin.roles.store') }}" method="POST">
                        {{ csrf_field() }}
                        @if(isset($role))
                            @method('PUT')
                        @endif

                        @if($errors->any())
                            <div class="alert alert-danger">{!! HTML::ul($errors->all()) !!}</div>
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Name" value="{{ isset($role) ? $role->name : '' }}" name="name">
                            </div>

                            @foreach($permissions as $permission_module => $permission_group)
                                <h4>{{ucfirst($permission_module)}}</h4>
                                @foreach($permission_group as $permission)
                                    <div class="">
                                        <label>
                                            <input type="checkbox" value="{{$permission->id}}"
                                                   name="permissions[]" {{isset($role) && $role->hasPermissionTo($permission->id) ? 'checked' : ''}}>
                                            {{ucfirst($permission->action)}}
                                        </label>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>

                        <button class="btn btn-primary" type="submit">Save</button>
                        <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
