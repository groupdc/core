<?php

namespace	Dcms\Core;

/**
*
* @author web <web@groupdc.be>
*/
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/resources/views'), 'dcms');
        $this->setupRoutes($this->app->router);
        // this  for conig
        $this->publishes([
      __DIR__.'/public/assets' => public_path('packages/Dcms/Core'),
      __DIR__.'/config/auth.php' => config_path('Dcms/Core/auth.php'),
      __DIR__.'/config/dcms_sidebar.php' => config_path('Dcms/Core/dcms_sidebar.php'),
   ]);

        if (!is_null(config('dcms.core'))) {
            $this->app['config']['dcms_sidebar'] = array_merge((array)$this->app["config"]["dcms_sidebar"], config('dcms.core.dcms_sidebar'));
            $this->app['config']['auth'] = array_merge_recursive((array)$this->app["config"]["auth"], config('dcms.core.auth'));
        }

        // Implicitly grant "administrator" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            if ($user->hasRole('administrator') || $user->hasRole('Administrator')) {
                return true;
            }
        });
    }
    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\Core\Http\Controllers'], function ($router) {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register()
    {
        $this->registerCore();
    }

    private function registerCore()
    {
        $this->app->bind('core', function ($app) {
            return new Core($app);
        });
    }
}
