<?php
namespace Dcms\Core\Database\Seeds;

use Dcms\Core\Models\Users\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator_role = Role::firstOrCreate(['name' => 'administrator']);
        Role::firstOrCreate(['name' => 'user']);

        $admin_user = User::where('name', 'admin')->first();
        $admin_user->assignRole($administrator_role);
    }
}
